class FeaturedProductsController < ApplicationController
  before_action :set_featured_product, only: %i[ show edit update destroy ]

  # GET /featured_products or /featured_products.json
  def index
    @featured_products = FeaturedProduct.all
  end

  # GET /featured_products/1 or /featured_products/1.json
  def show
  end

  # GET /featured_products/new
  def new
    @featured_product = FeaturedProduct.new
  end

  # GET /featured_products/1/edit
  def edit
  end

  # POST /featured_products or /featured_products.json
  def create
    @featured_product = FeaturedProduct.new(featured_product_params)

    respond_to do |format|
      if @featured_product.save
        format.html { redirect_to @featured_product, notice: "Featured product was successfully created." }
        format.json { render :show, status: :created, location: @featured_product }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @featured_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /featured_products/1 or /featured_products/1.json
  def update
    respond_to do |format|
      if @featured_product.update(featured_product_params)
        format.html { redirect_to @featured_product, notice: "Featured product was successfully updated." }
        format.json { render :show, status: :ok, location: @featured_product }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @featured_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /featured_products/1 or /featured_products/1.json
  def destroy
    @featured_product.destroy
    respond_to do |format|
      format.html { redirect_to featured_products_url, notice: "Featured product was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_featured_product
      @featured_product = FeaturedProduct.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def featured_product_params
      params.require(:featured_product).permit(:img_url, :img_title, :product_name)
    end
end
