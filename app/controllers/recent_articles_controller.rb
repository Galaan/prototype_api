class RecentArticlesController < ApplicationController
  before_action :set_recent_article, only: %i[ show edit update destroy ]

  # GET /recent_articles or /recent_articles.json
  def index
    @recent_articles = RecentArticle.all
  end

  # GET /recent_articles/1 or /recent_articles/1.json
  def show
  end

  # GET /recent_articles/new
  def new
    @recent_article = RecentArticle.new
  end

  # GET /recent_articles/1/edit
  def edit
  end

  # POST /recent_articles or /recent_articles.json
  def create
    @recent_article = RecentArticle.new(recent_article_params)

    respond_to do |format|
      if @recent_article.save
        format.html { redirect_to @recent_article, notice: "Recent article was successfully created." }
        format.json { render :show, status: :created, location: @recent_article }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @recent_article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recent_articles/1 or /recent_articles/1.json
  def update
    respond_to do |format|
      if @recent_article.update(recent_article_params)
        format.html { redirect_to @recent_article, notice: "Recent article was successfully updated." }
        format.json { render :show, status: :ok, location: @recent_article }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @recent_article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recent_articles/1 or /recent_articles/1.json
  def destroy
    @recent_article.destroy
    respond_to do |format|
      format.html { redirect_to recent_articles_url, notice: "Recent article was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recent_article
      @recent_article = RecentArticle.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def recent_article_params
      params.require(:recent_article).permit(:article_img, :publication_date, :publisher, :title)
    end
end
