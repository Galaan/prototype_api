json.extract! featured_product, :id, :img_url, :img_title, :product_name, :created_at, :updated_at
json.url featured_product_url(featured_product, format: :json)
