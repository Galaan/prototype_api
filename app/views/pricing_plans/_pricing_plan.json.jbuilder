json.extract! pricing_plan, :id, :title, :price, :description, :created_at, :updated_at
json.url pricing_plan_url(pricing_plan, format: :json)
