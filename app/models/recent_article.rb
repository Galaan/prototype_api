class RecentArticle
  include Mongoid::Document
  include Mongoid::Timestamps
  field :article_img, type: String
  field :publication_date, type: String
  field :publisher, type: String
  field :title, type: String
end
