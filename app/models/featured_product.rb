class FeaturedProduct
  include Mongoid::Document
  include Mongoid::Timestamps
  field :img_url, type: String
  field :img_title, type: String
  field :product_name, type: String
end
