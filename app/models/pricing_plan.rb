class PricingPlan
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :price, type: String
  field :description, type: String
end
