class LandingpageItem
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :description, type: String
  field :price, type: String
  field :rating, type: String
end
