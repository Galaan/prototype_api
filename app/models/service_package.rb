class ServicePackage
  include Mongoid::Document
  include Mongoid::Timestamps
  field :logo, type: String
  field :title, type: String
  field :detail, type: String
end
