require 'test_helper'

class LandingpageItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @landingpage_item = landingpage_items(:one)
  end

  test "should get index" do
    get landingpage_items_url
    assert_response :success
  end

  test "should get new" do
    get new_landingpage_item_url
    assert_response :success
  end

  test "should create landingpage_item" do
    assert_difference('LandingpageItem.count') do
      post landingpage_items_url, params: { landingpage_item: { description: @landingpage_item.description, name: @landingpage_item.name, price: @landingpage_item.price, rating: @landingpage_item.rating } }
    end

    assert_redirected_to landingpage_item_url(LandingpageItem.last)
  end

  test "should show landingpage_item" do
    get landingpage_item_url(@landingpage_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_landingpage_item_url(@landingpage_item)
    assert_response :success
  end

  test "should update landingpage_item" do
    patch landingpage_item_url(@landingpage_item), params: { landingpage_item: { description: @landingpage_item.description, name: @landingpage_item.name, price: @landingpage_item.price, rating: @landingpage_item.rating } }
    assert_redirected_to landingpage_item_url(@landingpage_item)
  end

  test "should destroy landingpage_item" do
    assert_difference('LandingpageItem.count', -1) do
      delete landingpage_item_url(@landingpage_item)
    end

    assert_redirected_to landingpage_items_url
  end
end
