require 'test_helper'

class FeaturedProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @featured_product = featured_products(:one)
  end

  test "should get index" do
    get featured_products_url
    assert_response :success
  end

  test "should get new" do
    get new_featured_product_url
    assert_response :success
  end

  test "should create featured_product" do
    assert_difference('FeaturedProduct.count') do
      post featured_products_url, params: { featured_product: { img_title: @featured_product.img_title, img_url: @featured_product.img_url, product_name: @featured_product.product_name } }
    end

    assert_redirected_to featured_product_url(FeaturedProduct.last)
  end

  test "should show featured_product" do
    get featured_product_url(@featured_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_featured_product_url(@featured_product)
    assert_response :success
  end

  test "should update featured_product" do
    patch featured_product_url(@featured_product), params: { featured_product: { img_title: @featured_product.img_title, img_url: @featured_product.img_url, product_name: @featured_product.product_name } }
    assert_redirected_to featured_product_url(@featured_product)
  end

  test "should destroy featured_product" do
    assert_difference('FeaturedProduct.count', -1) do
      delete featured_product_url(@featured_product)
    end

    assert_redirected_to featured_products_url
  end
end
