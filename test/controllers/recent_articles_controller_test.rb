require 'test_helper'

class RecentArticlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recent_article = recent_articles(:one)
  end

  test "should get index" do
    get recent_articles_url
    assert_response :success
  end

  test "should get new" do
    get new_recent_article_url
    assert_response :success
  end

  test "should create recent_article" do
    assert_difference('RecentArticle.count') do
      post recent_articles_url, params: { recent_article: { article_img: @recent_article.article_img, publication_date: @recent_article.publication_date, publisher: @recent_article.publisher, title: @recent_article.title } }
    end

    assert_redirected_to recent_article_url(RecentArticle.last)
  end

  test "should show recent_article" do
    get recent_article_url(@recent_article)
    assert_response :success
  end

  test "should get edit" do
    get edit_recent_article_url(@recent_article)
    assert_response :success
  end

  test "should update recent_article" do
    patch recent_article_url(@recent_article), params: { recent_article: { article_img: @recent_article.article_img, publication_date: @recent_article.publication_date, publisher: @recent_article.publisher, title: @recent_article.title } }
    assert_redirected_to recent_article_url(@recent_article)
  end

  test "should destroy recent_article" do
    assert_difference('RecentArticle.count', -1) do
      delete recent_article_url(@recent_article)
    end

    assert_redirected_to recent_articles_url
  end
end
