require "application_system_test_case"

class CustomerTestimoniesTest < ApplicationSystemTestCase
  setup do
    @customer_testimony = customer_testimonies(:one)
  end

  test "visiting the index" do
    visit customer_testimonies_url
    assert_selector "h1", text: "Customer Testimonies"
  end

  test "creating a Customer testimony" do
    visit customer_testimonies_url
    click_on "New Customer Testimony"

    fill_in "Testimony cust img", with: @customer_testimony.testimony_cust_img
    fill_in "Testimony cust name", with: @customer_testimony.testimony_cust_name
    fill_in "Testimony cust title", with: @customer_testimony.testimony_cust_title
    fill_in "Testimony detail", with: @customer_testimony.testimony_detail
    fill_in "Testimony logo", with: @customer_testimony.testimony_logo
    click_on "Create Customer testimony"

    assert_text "Customer testimony was successfully created"
    click_on "Back"
  end

  test "updating a Customer testimony" do
    visit customer_testimonies_url
    click_on "Edit", match: :first

    fill_in "Testimony cust img", with: @customer_testimony.testimony_cust_img
    fill_in "Testimony cust name", with: @customer_testimony.testimony_cust_name
    fill_in "Testimony cust title", with: @customer_testimony.testimony_cust_title
    fill_in "Testimony detail", with: @customer_testimony.testimony_detail
    fill_in "Testimony logo", with: @customer_testimony.testimony_logo
    click_on "Update Customer testimony"

    assert_text "Customer testimony was successfully updated"
    click_on "Back"
  end

  test "destroying a Customer testimony" do
    visit customer_testimonies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Customer testimony was successfully destroyed"
  end
end
