require "application_system_test_case"

class PricingPlansTest < ApplicationSystemTestCase
  setup do
    @pricing_plan = pricing_plans(:one)
  end

  test "visiting the index" do
    visit pricing_plans_url
    assert_selector "h1", text: "Pricing Plans"
  end

  test "creating a Pricing plan" do
    visit pricing_plans_url
    click_on "New Pricing Plan"

    fill_in "Description", with: @pricing_plan.description
    fill_in "Price", with: @pricing_plan.price
    fill_in "Title", with: @pricing_plan.title
    click_on "Create Pricing plan"

    assert_text "Pricing plan was successfully created"
    click_on "Back"
  end

  test "updating a Pricing plan" do
    visit pricing_plans_url
    click_on "Edit", match: :first

    fill_in "Description", with: @pricing_plan.description
    fill_in "Price", with: @pricing_plan.price
    fill_in "Title", with: @pricing_plan.title
    click_on "Update Pricing plan"

    assert_text "Pricing plan was successfully updated"
    click_on "Back"
  end

  test "destroying a Pricing plan" do
    visit pricing_plans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pricing plan was successfully destroyed"
  end
end
