  #documentation for prototype_api

  rails new prototype_api --skip-active-record
  cd prototype_api/
  nano Gemfile  #add gem mongoid with out version
  nano Gemfile  #add gem bson_ext with out version
  rails g mongoid:config
  rails g scaffold featured_product img_url:string img_title:string product_name:string  
  rails g scaffold landingpage_item name:string description:string price:string rating:string
  rails g scaffold service_package logo:string title:string detail:string
  rails g scaffold pricing_plan title:string price:string description:string  
  rails g scaffold customer_testimony testimony_logo:string testimony_detail:string testimony_cust_img:string testimony_cust_name:string testimony_cust_title:string 
  rails g scaffold recent_articles article_img:string publication_date:string publisher:string title:string
  rails routes
  rails db:migrate
  rails s




featured_product
- p_id
- img_url
- img_title
- product_name

landingpage_item
- id
- name
- description
- price
- rating

service_package
- id
- logo
- title
- detail

pricing_plan
- id
- title
- price
- description

customer_testimony
- testimony_id
- testimony_logo
- testimony_detail
- testimony_cust_img
- testimony_cust_name
- testimony_cust_title

recent_articles
- article_id
- article_img
- publication_date
- publisher
- title